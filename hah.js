//CONTROLS  ================================
$("#controlMute").click(function(){
  //find the enabled audio track
  for(track=0;track<localTracks.length;track++){
    thisTrack = localTracks[track];
    if(thisTrack.type=="audio"){
      //this is an enabled audio track. Toggle it;
      if(thisTrack.isMuted()){
        //unmute
        localTracks[track].unmute();
        $("#controlMute").html('<img src = "images/microphone.svg" alt="Mute" title = "Mute" />');
        console.log("unmuted");
      }
      else{
        //mute
        localTracks[track].mute();
        $("#controlMute").html('<img src = "images/microphone-off.svg" alt="Unmute" title = "Unute" />');
        console.log("muted");
      }
    }
  }
});

$("#controlCall").click(function(){
  //leave call
  unload();
});

$("#controlVideo").click(function(){
  //find the enabled video track
  for(track=0;track<localTracks.length;track++){
    thisTrack = localTracks[track];
    if(thisTrack.type=="video"){
      //this is an enabled video track. Toggle it;
      if(thisTrack.isMuted()){
        //unmute
        localTracks[track].unmute();
        $("#controlVideo").html('<img src = "images/video.svg" alt="Hide Video" title = "Hide Video" />');
        console.log("video shown");
      }
      else{
        //mute
        localTracks[track].mute();
        $("#controlVideo").html('<img src = "images/video-off.svg" alt="Show Video" title = "Show Video" />');
        console.log("video hidden");
      }
    }
  }
});

$("#cancelSettings").click(function(){
  if(username==""){
    alert("You need to enter a nickname");
  }
  else{
    $("#settingsBack").hide();
  }
});

$("#controlSettings").click(function(){
  $("#settingsBack").show();
});

$("#saveSettings").click(function(){
  if($("#usernameField").val()==""){
    alert("You need to enter a nickname");
  }
  else{
    newname = $("#usernameField").val() + "-" + $("input[name=headorhands]:checked").val();
    if(username != newname){
      //update hah object
      console.log("username changed");
      hah.usertype = $("input[name=headorhands]:checked").val().toLowerCase();
      for(user=0;user<hah.users.length;user++){
        if(hah.users[user]._displayName==username){
          hah.users[user]._displayName = newname;
        }
      }
      //update display name in room
      room.setDisplayName(newname);
      username = newname;
      //update/set cookie
      expires = new Date();
      expires.setHours(expires.getHours() + 1);
      document.cookie = "username=" + username + "; expires=" + expires + ";";
      usercookie = document.cookie;
      console.log("cookie set and expires " + expires);
      console.log(usercookie);
    }
    $("#settingsBack").hide();
  }
});

//==========================================


//called from example.js when the room is joined.
function setUpHead(){
  //get the username cookie if one is set, otherwise ask for a username.
  var usercookie = document.cookie;
  if(usercookie == ""){
    $("#settingsBack").show();
    username = "";
  }
  else{
    username = getCookie("username");
    $("#usernameField").val(username.replace("-HEAD","").replace("-HANDS",""));
    if(~username.indexOf("-HEAD")){
      $("#head").prop("checked","true");
    }
    else{
      $("#hands").prop("checked", "true");
    }
  }

  console.log("SETTING DISPLAY NAME===");
  room.setDisplayName(username);

  hah.users.push({_id:"local",_displayName:username});
}

function onUserLeft(id, user) {
    console.log('user left:');
    console.log(user);
    //remove from the hah user list
    for(i=0;i<hah.users.length;i++){
      if(hah.users[i]._id == id){
        hah.users.splice(i,1);
        //clean video element
        $("video").each(function(){
          if(~$(this).attr("id").indexOf(id + "video")){
            $(this).remove();
          }
        });
      }
    }
    if (!remoteTracks[id]) {
        return;
    }
    const tracks = remoteTracks[id];

    for (let i = 0; i < tracks.length; i++) {
        $(tracks[i].containers[0]).remove()
        tracks[i].detach($(`#${id}${tracks[i].getType()}`));
    }
    matchHeadsAndHands();
}

function onDisplayNameChange(userID, displayName){
  console.log(`Display name changed to ${userID} - ${displayName}`);
  for(i=0;i<hah.users.length;i++){
    if(hah.users[i]._id == userID){
      hah.users[i]._displayName = displayName;
      console.log("HAH: Updated displayName for user " + userID + " to " + displayName);
      matchHeadsAndHands();
      break;
    }
  }
}

function onUserJoined(id,user){
  console.log('user joined:');
  console.log(user);
  hah.users.push(user);
  $("#choosehead").append(user._displayName);
  remoteTracks[id] = [];
  if(hah.usertype=="hands") mutePage();
  matchHeadsAndHands();
}


//====================================================

function mutePage() {
    var elems = document.querySelectorAll("video, audio");

    [].forEach.call(elems, function(elem) { $(elem).data("muted",true); });

}

function updateHeadList(){
  $("#choosehead").html("<option>Choose a Head</option>");
  for(i=0;i<hah.users.length;i++){
    if(hah.users[i]._id != "local" && ~hah.users[i]._displayName.indexOf("-HEAD")){
      $("#choosehead").append("<option>"+hah.users[i]._displayName.replace("-HEAD","")+"</option>");
    }
  }
}

function linkToHead(headName){
  newname = headName.replace("-HEAD","");
  newname += "-HANDS";
  alert("This device's name is " + newname);
  room.setDisplayName(newname);
  $("#cover").hide();
}

function matchHeadsAndHands(){
  //heads get the full display update
  if(hah.usertype=="head"){
    //set display names
    console.log("matching heads and hands");
    usercount = 1;
    $("#user1").children("p").first().html("User 1");
    $("#user2").children("p").first().html("User 2");
    $("#user3").children("p").first().html("User 3");
    $("#user4").children("p").first().html("User 4");
    for(i=0;i<hah.users.length;i++){
      if(hah.users[i]._displayName != undefined && ~hah.users[i]._displayName.indexOf("-HEAD")){
        $("#user"+(i+1)).children("p").first().html(hah.users[i]._displayName.replace("-HEAD",""));
        usercount++;
      }
    }

    //1) look for the hah.users that matches the UserEndpointId of the track
    //2) Get the nickname of the user minus the suffix
    //3) look through all the userColumn elements for one with the nickname title.
    //4) push the container for the track to that element.

    //local and remote tracks are stored in different formats so we loop through both differently
    //and send them to pushTrackToBox once they're all in the same format.
    for(i=0;i<localTracks.length;i++){
      if(localTracks[i].type=="video" && !localTracks[i].isMuted()){
        pushTrackToBox(localTracks[i]);
        break;
      }
    }
    for(key in remoteTracks){
      tracks = remoteTracks[key];
      hideFeed = false;
      for(i=tracks.length-1;i>-1;i--){
        if(tracks[i].type=="video"){
          if(hideFeed || tracks[i].isMuted()){
            //latest video track already found. Hide any others
            $(tracks[i].containers[0]).hide();
          }
          else{
            //make sure this video is visible
            $(tracks[i].containers[0]).show();
            //put it in the right box.
            hideFeed = pushTrackToBox(tracks[i]);
          }
        }
      }
    }
  }
  if(hah.usertype=="hands"){
    //hands just make sure everything is hidden and muted.
    $("video").remove();
    $("audio").remove();
  }
}

function pushTrackToBox(track){
  //local tracks don't have an endpoint id so we create one called local
  if(track.ownerEndpointId == undefined) track.ownerEndpointId = "local";

  //loop through all the uesers to find the displayname that matches the track's owner id
  for(useri=0;useri<hah.users.length;useri++){
    if(hah.users[useri]._id == track.ownerEndpointId){
      //found the user the track belongs to. get their nick
      nick = hah.users[useri]._displayName;
      nick = nick.replace("-HEAD","").replace("-HANDS","");
      $(".userColumn").each(function(){
        if($(this).children().first().html()==nick){
          //found the correct box. add it.
          if(~hah.users[useri]._displayName.indexOf("-HEAD")){
            $(track.containers[0]).removeClass("handsVideo");
            $(track.containers[0]).addClass("headVideo");
            $(this).find(".userBox-head")[0].append(track.containers[0]);
          }
          else{
            $(track.containers[0]).removeClass("headVideo");
            $(track.containers[0]).addClass("handsVideo");
            $(this).find(".userBox-hands")[0].append(track.containers[0]);
          }
          return true;
        }
      });
    }
  }
}

var myloopthatnooneelseusesforsure = setInterval(function(){
  try{
    matchHeadsAndHands();
    if(hah.usertype=="hands"){
      mutePage();
      if(hah.automuted==false){
        for(track=0;track<localTracks.length;track++){
          if(localTracks[track].type=="audio"){
            localTracks[track].mute();
            $("#controlMute").html('<img src = "images/microphone-off.svg" alt="Unmute" title = "Unute" />');
            hah.automuted=true;
          }
        }
      }
      $(".userColumn").hide();
      $("#handsClientMessage").show();
    }
    else{
      $(".userColumn").show();
      $("#handsClientMessage").hide();
    }
  }
  catch(e){
    console.log(e);
  }
},2000);

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
