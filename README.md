# HeadandHands

Head and hands is a video conferencing system designed to enable people to engage in open-ended creative activity together online.  It differs from standard videocall systems by showing participant’s hands as well as their faces. It is designed to enable learner agency, a sense of shared experience, and the transmission of new ideas in a shared creative context. We aspire to make it a medium for enabling online social tinkering and collective creativity.

You can find a more detailed specification (work in progress) here:
https://cloud.amosamos.net/index.php/s/nkTjLdHdMTK3eSm
